/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import testedistribuicoes.*;
import java.awt.*;
import java.awt.event.*;
import java.text.DecimalFormat;
import javax.swing.*;
import javax.swing.border.*;

public class DialogDifMediaPopula extends JDialog {

    private JTextField txtMedia1, txtDesvio1, txtAmostra1, txtMedia2, txtDesvio2, txtAmostra2;
    private JComboBox jcb;

    private static final int DIALOGO_DESVIO_X = 100, DIALOGO_DESVIO_Y = 100;
    private static final Dimension LABEL_TAMANHO = new JLabel("Intervalo de Confiança(%) =").getPreferredSize();
    private static final String IMAGEM_IC_DIF_MED = "DifMediaas.PNG";

    DecimalFormat df = new DecimalFormat("#.####");

    public DialogDifMediaPopula(JFrame pai) {

        super(pai, "Diferença de Médias de Populações", true);

        GridLayout g1 = new GridLayout(10, 1);
        setLayout(g1);

        JPanel p1 = criarPainelA1();
        JPanel p2 = criarPainelMedia1();
        JPanel p3 = criarPainelDesvio1();
        JPanel p4 = criarPainelAmostra1();
        JPanel p5 = criarPainelA2();
        JPanel p6 = criarPainelMedia2();
        JPanel p7 = criarPainelDesvio2();
        JPanel p8 = criarPainelAmostra2();
        JPanel p9 = criarPainelIC();
        JPanel p10 = criarPainelBotoes();

        add(p1);
        add(p2);
        add(p3);
        add(p4);
        add(p5);
        add(p6);
        add(p7);
        add(p8);
        add(p9);
        add(p10);

        setLocation(pai.getX() + DIALOGO_DESVIO_X, pai.getY() + DIALOGO_DESVIO_Y);
        pack();
        setResizable(false);
        setVisible(true);
    }

    private JPanel criarPainelA1() {
        JLabel lbl1 = new JLabel("AMOSTRA A", JLabel.RIGHT);
        lbl1.setPreferredSize(LABEL_TAMANHO);

        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl1);

        return p;
    }

    private JPanel criarPainelDesvio1() {
        JLabel lbl = new JLabel("Desvio Padrão = \u03C3 =", JLabel.RIGHT);
        lbl.setPreferredSize(LABEL_TAMANHO);

        final int CAMPO_LARGURA = 5;
        txtDesvio1 = new JTextField(CAMPO_LARGURA);
        txtDesvio1.requestFocus();

        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl);
        p.add(txtDesvio1);

        return p;
    }

    private JPanel criarPainelBotoes() {
        JButton btnOK = criarBotaoOK();
        getRootPane().setDefaultButton(btnOK);

        JButton btnCancelar = criarBotaoCancelar();

        JPanel p = new JPanel();
        final int MARGEM_SUPERIOR = 0, MARGEM_INFERIOR = 10;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 10;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));
        p.add(btnOK);
        p.add(btnCancelar);

        return p;
    }

    private JButton criarBotaoOK() {
        JButton btn = new JButton("OK");
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    double media1 = Double.parseDouble(txtMedia1.getText());

                    double desvio1 = Double.parseDouble(txtDesvio1.getText());

                    int amostra1 = Integer.parseInt(txtAmostra1.getText());

                    double z = Double.parseDouble((String) jcb.getSelectedItem());

                    IntervalosConfianca ic1 = new IntervalosConfianca(amostra1, media1,
                            desvio1, jcb.getSelectedIndex());

                    if (media1 < 0 || desvio1 < 0 || amostra1 < 0) {
                        throw new ValorNegDifPopException();
                    }

                    double media2 = Double.parseDouble(txtMedia2.getText());

                    double desvio2 = Double.parseDouble(txtDesvio2.getText());

                    int amostra2 = Integer.parseInt(txtAmostra2.getText());

                    IntervalosConfianca ic2 = new IntervalosConfianca(amostra2, media2,
                            desvio2, jcb.getSelectedIndex());

                    if (media2 < 0 || desvio2 < 0 || amostra2 < 0) {
                        throw new ValorNegDifPopException();
                    }

                    ImageIcon icon = new ImageIcon(IMAGEM_IC_DIF_MED);

                    JOptionPane.showMessageDialog(DialogDifMediaPopula.this,
                            new JLabel("=" + new DiferencaIC().calcDiferenca(ic1, ic2), icon, JLabel.LEFT),
                            "Diferença de médias de população",
                            JOptionPane.DEFAULT_OPTION);

                    dispose();
                } catch (NumberFormatException excecao) {
                    JOptionPane.showMessageDialog(DialogDifMediaPopula.this,
                            "Tem que introduzir um valor numérico.",
                            "Diferença de médias de população",
                            JOptionPane.ERROR_MESSAGE);

                } catch (ValorNegDifPopException excecao) {
                    JOptionPane.showMessageDialog(DialogDifMediaPopula.this,
                            "( E(X)= \u00B5 ) ≥ 0 ",
                            "Cálculo de Distribuição de poisson",
                            JOptionPane.WARNING_MESSAGE);
                }
            }
        });
        return btn;
    }

    private JPanel criarPainelAmostra1() {

        JLabel lbl = new JLabel("Amostra = n =", JLabel.RIGHT);
        lbl.setPreferredSize(LABEL_TAMANHO);

        final int CAMPO_LARGURA = 5;
        txtAmostra1 = new JTextField(CAMPO_LARGURA);
        txtAmostra1.requestFocus();

        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl);
        p.add(txtAmostra1);

        return p;

    }

    private JPanel criarPainelMedia1() {
        JLabel lbl = new JLabel("E(X)= \u00B5 =", JLabel.RIGHT);
        lbl.setPreferredSize(LABEL_TAMANHO);

        final int CAMPO_LARGURA = 5;
        txtMedia1 = new JTextField(CAMPO_LARGURA);
        txtMedia1.requestFocus();

        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl);
        p.add(txtMedia1);
        return p;
    }

    private JPanel criarPainelA2() {
        JLabel lbl1 = new JLabel("AMOSTRA B", JLabel.RIGHT);
        lbl1.setPreferredSize(LABEL_TAMANHO);

        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl1);

        return p;
    }

    private JPanel criarPainelMedia2() {
        JLabel lbl = new JLabel("E(X)= \u00B5 =", JLabel.RIGHT);
        lbl.setPreferredSize(LABEL_TAMANHO);

        final int CAMPO_LARGURA = 5;
        txtMedia2 = new JTextField(CAMPO_LARGURA);
        txtMedia2.requestFocus();

        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl);
        p.add(txtMedia2);
        return p;
    }

    private JPanel criarPainelDesvio2() {
        JLabel lbl = new JLabel("Desvio Padrão = \u03C3 =", JLabel.RIGHT);
        lbl.setPreferredSize(LABEL_TAMANHO);

        final int CAMPO_LARGURA = 5;
        txtDesvio2 = new JTextField(CAMPO_LARGURA);
        txtDesvio2.requestFocus();

        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl);
        p.add(txtDesvio2);

        return p;
    }

    private JPanel criarPainelAmostra2() {

        JLabel lbl = new JLabel("Amostra = n =", JLabel.RIGHT);
        lbl.setPreferredSize(LABEL_TAMANHO);

        final int CAMPO_LARGURA = 5;
        txtAmostra2 = new JTextField(CAMPO_LARGURA);
        txtAmostra2.requestFocus();

        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl);
        p.add(txtAmostra2);

        return p;

    }

    private JButton criarBotaoCancelar() {
        JButton btn = new JButton("Cancelar");
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        return btn;
    }

    private JPanel criarPainelIC() {

        JLabel lbl = new JLabel("Intervalo de Confiança(%) =", JLabel.RIGHT);
        lbl.setPreferredSize(LABEL_TAMANHO);
        String[] op = new String[]{"99.73", "99", "98", "96", "95.45", "95", "90"};
        jcb = new JComboBox(op);

        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl);
        p.add(jcb);

        return p;
    }

}
