/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import testedistribuicoes.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;

public class DialogMediaPropor extends JDialog {

    private JTextField txtFavoravel, txtAmostra;
    private JComboBox jcb;

    private static final int DIALOGO_DESVIO_X = 100, DIALOGO_DESVIO_Y = 100;
    private static final Dimension LABEL_TAMANHO = new JLabel("Intervalo de Confiança(%) =").getPreferredSize();

    private static final String IMAGEM_IC_MED_PROPO = "mediaPropor.PNG";

    public DialogMediaPropor(JFrame pai) {

        super(pai, "Média de Proporções", true);

        GridLayout g1 = new GridLayout(4, 1);
        setLayout(g1);

        JPanel p1 = criarPainelFavoraveis();
        JPanel p2 = criarPainelAmostra();
        JPanel p3 = criarPainelIC();
        JPanel p4 = criarPainelBotoes();

        add(p1);
        add(p2);
        add(p3);
        add(p4);

        setLocation(pai.getX() + DIALOGO_DESVIO_X, pai.getY() + DIALOGO_DESVIO_Y);
        pack();
        setResizable(false);
        setVisible(true);
    }

    private JPanel criarPainelAmostra() {

        JLabel lbl = new JLabel("Amostra = n =", JLabel.RIGHT);
        lbl.setPreferredSize(LABEL_TAMANHO);

        final int CAMPO_LARGURA = 5;
        txtAmostra = new JTextField(CAMPO_LARGURA);
        txtAmostra.requestFocus();

        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl);
        p.add(txtAmostra);

        return p;

    }

    private JPanel criarPainelFavoraveis() {
        JLabel lbl = new JLabel("Nº de Casos Favoráveis =", JLabel.RIGHT);
        lbl.setPreferredSize(LABEL_TAMANHO);

        final int CAMPO_LARGURA = 5;
        txtFavoravel = new JTextField(CAMPO_LARGURA);
        txtFavoravel.requestFocus();

        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl);
        p.add(txtFavoravel);
        return p;
    }

    private JButton criarBotaoOK() {
        JButton btn = new JButton("OK");
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    int favoravel = Integer.parseInt(txtFavoravel.getText());
                    if (favoravel < 0) {
                        throw new NumFavoravelInvalidoException();
                    }
                    int amostra = Integer.parseInt(txtAmostra.getText());
                    if (amostra < 0) {
                        throw new ValorAmostraNegativoException();
                    }
                    Proporcao ic = new Proporcao(amostra, favoravel, jcb.getSelectedIndex());
                    ImageIcon icon = new ImageIcon(IMAGEM_IC_MED_PROPO);
                    JOptionPane.showMessageDialog(DialogMediaPropor.this,
                            new JLabel("="+ ic.calcIC(), icon, JLabel.LEFT),
                            "Média de proporções", 
                            JOptionPane.DEFAULT_OPTION);

                    dispose();
                } catch (NumberFormatException excecao) {
                    JOptionPane.showMessageDialog(DialogMediaPropor.this,
                            "Tem que introduzir um valor numérico.",
                            "Média de proporções",
                            JOptionPane.WARNING_MESSAGE);

                } catch (NumFavoravelInvalidoException excecao) {
                    JOptionPane.showMessageDialog(DialogMediaPropor.this,
                            "O número de casos favoráveis deve ser > 0!",
                            "Média de proporções",
                            JOptionPane.WARNING_MESSAGE);
                    txtFavoravel.requestFocus();
                } catch (ValorAmostraNegativoException excecao) {
                    JOptionPane.showMessageDialog(DialogMediaPropor.this,
                            excecao.getMessage(),
                            "Média de proporções",
                            JOptionPane.WARNING_MESSAGE);
                }
            }
        });
        return btn;
    }

    private JPanel criarPainelBotoes() {
        JButton btnOK = criarBotaoOK();
        getRootPane().setDefaultButton(btnOK);

        JButton btnCancelar = criarBotaoCancelar();

        JPanel p = new JPanel();
        final int MARGEM_SUPERIOR = 0, MARGEM_INFERIOR = 10;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 10;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));
        p.add(btnOK);
        p.add(btnCancelar);

        return p;
    }

    private JButton criarBotaoCancelar() {
        JButton btn = new JButton("Cancelar");
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        return btn;
    }

    private JPanel criarPainelIC() {

        JLabel lbl = new JLabel("Intervalo de Confiança(%) =", JLabel.RIGHT);
        lbl.setPreferredSize(LABEL_TAMANHO);
        String[] op = new String[]{"99.73%", "99%", "98%", "96%", "95.45%", "95%", "90%"};
        jcb = new JComboBox(op);

        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl);
        p.add(jcb);

        return p;
    }

}
