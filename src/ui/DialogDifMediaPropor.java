package ui;

import java.awt.*;
import java.awt.event.*;
import java.text.DecimalFormat;
import javax.swing.*;
import javax.swing.border.*;
import testedistribuicoes.*;

public class DialogDifMediaPropor extends JDialog {

    private JTextField txtFavoraveis1, txtAmostra1, txtFavoraveis2, txtAmostra2;
    private JComboBox jcb;

    private static final int DIALOGO_DESVIO_X = 100, DIALOGO_DESVIO_Y = 100;
    private static final Dimension LABEL_TAMANHO = new JLabel("Intervalo de Confiança(%) =").getPreferredSize();
    private static final String IMAGEM_IC_DIF_PROP = "difPropor.PNG";
    DecimalFormat df = new DecimalFormat("#.####");

    public DialogDifMediaPropor(JFrame pai) {

        super(pai, "Diferença de Médias de Proporções", true);

        GridLayout g1 = new GridLayout(8, 1);
        setLayout(g1);

        JPanel p1 = criarPainelA1();
        JPanel p2 = criarPainelFavoraveis1();
        JPanel p3 = criarPainelAmostra1();
        JPanel p4 = criarPainelA2();
        JPanel p5 = criarPainelFavoraveis2();
        JPanel p6 = criarPainelAmostra2();
        JPanel p7 = criarPainelIC();
        JPanel p8 = criarPainelBotoes();

        add(p1);
        add(p2);
        add(p3);
        add(p4);
        add(p5);
        add(p6);
        add(p7);
        add(p8);

        setLocation(pai.getX() + DIALOGO_DESVIO_X, pai.getY() + DIALOGO_DESVIO_Y);
        pack();
        setResizable(false);
        setVisible(true);
    }

    private JPanel criarPainelA1() {
        JLabel lbl1 = new JLabel("PROPORÇÃO A", JLabel.RIGHT);
        lbl1.setPreferredSize(LABEL_TAMANHO);

        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl1);

        return p;
    }

    private JPanel criarPainelBotoes() {
        JButton btnOK = criarBotaoOK();
        getRootPane().setDefaultButton(btnOK);

        JButton btnCancelar = criarBotaoCancelar();

        JPanel p = new JPanel();
        final int MARGEM_SUPERIOR = 0, MARGEM_INFERIOR = 10;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 10;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));
        p.add(btnOK);
        p.add(btnCancelar);

        return p;
    }

    private JButton criarBotaoOK() {
        JButton btn = new JButton("OK");
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    int favoraveis1 = Integer.parseInt(txtFavoraveis1.getText());
                    int favoraveis2 = Integer.parseInt(txtFavoraveis2.getText());
                    int amostra1 = Integer.parseInt(txtAmostra1.getText());
                    int amostra2 = Integer.parseInt(txtAmostra2.getText());
                    if (favoraveis1 < 0 || favoraveis2 < 0) {
                        throw new poissonMediaInvalidoException();
                    }
                    if (amostra1 == 0 || amostra2 == 0) {
                        throw new ValorAmostraNegativoException();
                    }
                    Proporcao p1 = new Proporcao(amostra1, favoraveis1, jcb.getSelectedIndex());
                    Proporcao p2 = new Proporcao(amostra2, favoraveis2, jcb.getSelectedIndex());
                    
                    ImageIcon icon = new ImageIcon(IMAGEM_IC_DIF_PROP);
                    
                    JOptionPane.showMessageDialog(DialogDifMediaPropor.this,
                            new JLabel("=" + new DiferencaProporcao().calcDiferencaProporcao(p1, p2), icon, JLabel.LEFT),
                            "Diferença de Proporções",
                            JOptionPane.DEFAULT_OPTION);
                    dispose();
                } catch (NumberFormatException excecao) {
                    JOptionPane.showMessageDialog(DialogDifMediaPropor.this,
                            "Tem de introduzir um numero.",
                            "Diferença de Proporções",
                            JOptionPane.WARNING_MESSAGE);
                    txtAmostra1.requestFocus();

                } catch (poissonMediaInvalidoException ex) {
                    JOptionPane.showMessageDialog(DialogDifMediaPropor.this,
                            "O valor não deve ser negativo",
                            "Diferença de Proporções",
                            JOptionPane.WARNING_MESSAGE);
                } catch (ValorAmostraNegativoException ex) {
                    JOptionPane.showMessageDialog(DialogDifMediaPropor.this,
                            "O valor não deve ser 0.",
                            "Diferença de Proporções",
                            JOptionPane.WARNING_MESSAGE);
                }
            }
        });
        return btn;
    }

    private JPanel criarPainelAmostra1() {

        JLabel lbl = new JLabel("Amostra = n =", JLabel.RIGHT);
        lbl.setPreferredSize(LABEL_TAMANHO);

        final int CAMPO_LARGURA = 5;
        txtAmostra1 = new JTextField(CAMPO_LARGURA);
        txtAmostra1.requestFocus();

        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl);
        p.add(txtAmostra1);

        return p;

    }

    private JPanel criarPainelFavoraveis1() {
        JLabel lbl = new JLabel("Nº Casos Favoráveis=", JLabel.RIGHT);
        lbl.setPreferredSize(LABEL_TAMANHO);

        final int CAMPO_LARGURA = 5;
        txtFavoraveis1 = new JTextField(CAMPO_LARGURA);
        txtFavoraveis1.requestFocus();

        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl);
        p.add(txtFavoraveis1);
        return p;
    }

    private JPanel criarPainelA2() {
        JLabel lbl1 = new JLabel("PROPORÇÃO B", JLabel.RIGHT);
        lbl1.setPreferredSize(LABEL_TAMANHO);

        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl1);

        return p;
    }

    private JPanel criarPainelFavoraveis2() {
        JLabel lbl = new JLabel("Nº Casos Favoráveis=", JLabel.RIGHT);
        lbl.setPreferredSize(LABEL_TAMANHO);

        final int CAMPO_LARGURA = 5;
        txtFavoraveis2 = new JTextField(CAMPO_LARGURA);
        txtFavoraveis2.requestFocus();

        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl);
        p.add(txtFavoraveis2);
        return p;
    }

    private JPanel criarPainelAmostra2() {

        JLabel lbl = new JLabel("Amostra = n =", JLabel.RIGHT);
        lbl.setPreferredSize(LABEL_TAMANHO);

        final int CAMPO_LARGURA = 5;
        txtAmostra2 = new JTextField(CAMPO_LARGURA);
        txtAmostra2.requestFocus();

        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl);
        p.add(txtAmostra2);

        return p;

    }

    private JButton criarBotaoCancelar() {
        JButton btn = new JButton("Cancelar");
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        return btn;
    }

    private JPanel criarPainelIC() {

        JLabel lbl = new JLabel("Intervalo de Confiança(%) =", JLabel.RIGHT);
        lbl.setPreferredSize(LABEL_TAMANHO);
        String[] op = new String[]{"99.73%", "99%", "98%", "96%", "95.45%", "95%", "90%"};
        jcb = new JComboBox(op);

        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl);
        p.add(jcb);

        return p;
    }

}
