package ui;

import testedistribuicoes.*;
import java.awt.*;
import java.awt.event.*;
import java.text.DecimalFormat;
import javax.swing.*;
import javax.swing.border.*;

public class DialogPoisson extends JDialog {

    private static final String IMAGEM_POISSON_SOMADA = "PoissonSomada.PNG";
    private static final String IMAGEM_POISSON_DIST = "PoissonDistribui.png";
    private static final String IMAGEM_DIST_POISS_PROB = "PoissonProbabil.png";
    
    private JTextField txtProbabilidade, txtValorX;
    private JComboBox jcb;

    private static final int DIALOGO_DESVIO_X = 100, DIALOGO_DESVIO_Y = 100;
    private static final Dimension LABEL_TAMANHO = new JLabel("Valores de X a calcular:").getPreferredSize();
    
    DecimalFormat df = new DecimalFormat("#.####"); 

    public DialogPoisson(JFrame pai) {

        super(pai, "Distribuição de Poisson", true);

        GridLayout g1 = new GridLayout(3, 1);
        setLayout(g1);

        JPanel p1 = criarPainelProbabilidade();
        JPanel p2 = criarPainelCalculo();
        JPanel p3 = criarPainelBotoes();

        add(p1);
        add(p2);
        add(p3);

        setLocation(pai.getX() + DIALOGO_DESVIO_X, pai.getY() + DIALOGO_DESVIO_Y);
        pack();
        setResizable(false);
        setVisible(true);
    }

    private JPanel criarPainelProbabilidade() {
        JLabel lbl = new JLabel("E(X)= \u00B5 =", JLabel.RIGHT);
        lbl.setPreferredSize(LABEL_TAMANHO);

        final int CAMPO_LARGURA = 5;
        txtProbabilidade = new JTextField(CAMPO_LARGURA);
        txtProbabilidade.requestFocus();

        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl);
        p.add(txtProbabilidade);
        return p;
    }

    private JButton criarBotaoCancelar() {
        JButton btn = new JButton("Cancelar");
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        return btn;
    }

    private JPanel criarPainelCalculo() {
        JLabel lbl = new JLabel("Valores de X a calcular:", JLabel.RIGHT);
        lbl.setPreferredSize(LABEL_TAMANHO);
        String[] op = new String[]{"≥", "≤", "="};
        jcb = new JComboBox(op);

        final int CAMPO_LARGURA = 5;
        txtValorX = new JTextField(CAMPO_LARGURA);
        txtValorX.requestFocus();

        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl);
        p.add(jcb);
        p.add(txtValorX);

        return p;
    }

    private JPanel criarPainelBotoes() {
        JButton btnOK = criarBotaoOK();
        getRootPane().setDefaultButton(btnOK);

        JButton btnCancelar = criarBotaoCancelar();

        JPanel p = new JPanel();
        final int MARGEM_SUPERIOR = 0, MARGEM_INFERIOR = 10;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 10;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));
        p.add(btnOK);
        p.add(btnCancelar);

        return p;
    }

    private JButton criarBotaoOK() {
        JButton btn = new JButton("OK");
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    double probabilidade = Double.parseDouble(txtProbabilidade.getText());
                    if (probabilidade < 0) {
                        throw new poissonMediaInvalidoException();
                    }
                    int valorX = Integer.parseInt(txtValorX.getText());
                    Poisson p = new Poisson(probabilidade, valorX);
                    
                    if (jcb.getSelectedIndex()==0){
                        ImageIcon icon = new ImageIcon(IMAGEM_POISSON_SOMADA);
                        JOptionPane.showMessageDialog(DialogPoisson.this,
                            new JLabel("="+df.format(p.calcDistSomada()), icon, JLabel.LEFT),
                            "Distribuição Binomial Somada", 
                            JOptionPane.DEFAULT_OPTION);    

                    }
                    
                    if (jcb.getSelectedIndex()==1){
                        ImageIcon icon = new ImageIcon(IMAGEM_POISSON_DIST);
                        JOptionPane.showMessageDialog(DialogPoisson.this,
                            new JLabel("="+df.format(p.calcDistribuicao()), icon, JLabel.LEFT),
                            "Distribuição Poisson Função Distribuição", 
                            JOptionPane.DEFAULT_OPTION);

                    }
                    
                    if (jcb.getSelectedIndex()==2){
                        ImageIcon icon = new ImageIcon(IMAGEM_DIST_POISS_PROB);
                        JOptionPane.showMessageDialog(DialogPoisson.this,
                            new JLabel("="+df.format(p.calcProbabilidade()), icon, JLabel.LEFT),
                            "Distribuição Binomial Função Probabilidade", 
                            JOptionPane.DEFAULT_OPTION);

                    }
                    
                    dispose();
                } catch (NumberFormatException excecao) {
                    JOptionPane.showMessageDialog(DialogPoisson.this,
                            "Tem que introduzir um valor numérico.",
                            "Cálculo de Distribuição Poisson",
                            JOptionPane.WARNING_MESSAGE);
                    txtProbabilidade.requestFocus();
//                } catch (probabilidadeInvalidoException excecao) {
//                    JOptionPane.showMessageDialog(DialogPoisson.this,
//                            "O valor da probabilidade deve pertencer ao intervalo [0;1]!",
//                            "Cálculo de Distribuição de poisson",
//                            JOptionPane.WARNING_MESSAGE);
//                    txtProbabilidade.requestFocus();
                } catch (poissonMediaInvalidoException ex) {
                    JOptionPane.showMessageDialog(DialogPoisson.this,
                            "( E(X)= U+03BC ) ≥ 0 ",
                            "Cálculo de Distribuição de poisson",
                            JOptionPane.WARNING_MESSAGE);
                }
            }
        });
        return btn;
    }
}
