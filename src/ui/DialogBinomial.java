/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import java.awt.*;
import java.awt.event.*;
import java.text.DecimalFormat;
import javax.swing.*;
import javax.swing.border.*;
import testedistribuicoes.*;

public class DialogBinomial extends JDialog {

    private JTextField txtProbabilidade, txtNumProvas, txtValorX;
    private JComboBox jcb;

    private static final int DIALOGO_DESVIO_X = 100, DIALOGO_DESVIO_Y = 100;
    private static final Dimension LABEL_TAMANHO = new JLabel("Valores de X a calcular:").getPreferredSize();
    
    private static final String IMAGEM_DIST_BIN_SOMADA =  "BinomialSomada.png";
    private static final String IMAGEM_DIST_BIN_DIST =  "BinomialDistribui.PNG";
    private static final String IMAGEM_DIST_BIN_PROB =  "BinomialProbabi.PNG";
    
    DecimalFormat df = new DecimalFormat("#.####"); 
    
    public DialogBinomial(Frame pai) {

        super(pai, "Distribuição Binomial", true);

        GridLayout g1 = new GridLayout(4, 1);
        setLayout(g1);

        JPanel p1 = criarPainelNumProvas();
        JPanel p2 = criarPainelProbabilidade();
        JPanel p3 = criarPainelCalculo();
        JPanel p4 = criarPainelBotoes();

        add(p1);
        add(p2);
        add(p3);
        add(p4);

        setLocation(pai.getX() + DIALOGO_DESVIO_X, pai.getY() + DIALOGO_DESVIO_Y);
        pack();
        setResizable(false);
        setVisible(true);
    }

    private JPanel criarPainelProbabilidade() {
        JLabel lbl = new JLabel("Probabilidade:", JLabel.RIGHT);
        lbl.setPreferredSize(LABEL_TAMANHO);

        final int CAMPO_LARGURA = 5;
        txtProbabilidade = new JTextField(CAMPO_LARGURA);
        txtProbabilidade.requestFocus();

        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl);
        p.add(txtProbabilidade);
        return p;
    }

    private JPanel criarPainelNumProvas() {
        JLabel lbl = new JLabel("Numero de provas:", JLabel.RIGHT);
        lbl.setPreferredSize(LABEL_TAMANHO);

        final int CAMPO_LARGURA = 5;
        txtNumProvas = new JTextField(CAMPO_LARGURA);
        txtNumProvas.requestFocus();

        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl);
        p.add(txtNumProvas);

        return p;
    }

    private JPanel criarPainelBotoes() {
        JButton btnOK = criarBotaoOK();
        getRootPane().setDefaultButton(btnOK);

        JButton btnCancelar = criarBotaoCancelar();

        JPanel p = new JPanel();
        final int MARGEM_SUPERIOR = 0, MARGEM_INFERIOR = 10;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 10;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));
        p.add(btnOK);
        p.add(btnCancelar);

        return p;
    }

    private JButton criarBotaoOK() {
        JButton btn = new JButton("OK");
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    int numProvas = Integer.parseInt(txtNumProvas.getText());
                    if (numProvas < 0) {
                        throw new numProvasInvalidoException();
                    }
                    double probabilidade = Double.parseDouble(txtProbabilidade.getText());
                    if (probabilidade < 0 || probabilidade > 1) {
                        throw new probabilidadeInvalidoException();
                    }
                    int valorX = Integer.parseInt(txtValorX.getText());
                    Binomial b = new Binomial(probabilidade, numProvas, valorX);
                    
                    
                    
                    if (jcb.getSelectedIndex()==0){
                        ImageIcon icon = new ImageIcon(IMAGEM_DIST_BIN_SOMADA);
                        JOptionPane.showMessageDialog(DialogBinomial.this,
                            new JLabel("="+df.format(b.calcDistSomada()), icon, JLabel.LEFT),
                            "Binomial Somada", 
                            JOptionPane.DEFAULT_OPTION);    

                    }
                    
                    if (jcb.getSelectedIndex()==1){
                        ImageIcon icon = new ImageIcon(IMAGEM_DIST_BIN_DIST);
                        JOptionPane.showMessageDialog(DialogBinomial.this,
                            new JLabel("="+df.format(b.calcDistribuicao()), icon, JLabel.LEFT),
                            "Binomial Função Distribuição", 
                            JOptionPane.DEFAULT_OPTION);

                    }
                    
                    if (jcb.getSelectedIndex()==2){
                        ImageIcon icon = new ImageIcon(IMAGEM_DIST_BIN_PROB);
                        JOptionPane.showMessageDialog(DialogBinomial.this,
                            new JLabel("="+df.format(b.calcProbabilidade()), icon, JLabel.LEFT),
                            "Binomial Função Probabilidade", 
                            JOptionPane.DEFAULT_OPTION);

                    }
                    
                    dispose();
                } catch (NumberFormatException excecao) {
                    JOptionPane.showMessageDialog(DialogBinomial.this,
                            "Tem que introduzir um valor numérico no numero de provas.",
                            "Cálculo de Distribuição Binomial",
                            JOptionPane.WARNING_MESSAGE);
                    txtNumProvas.requestFocus();
                } catch (numProvasInvalidoException excecao) {
                    JOptionPane.showMessageDialog(DialogBinomial.this,
                            "O numero de provas deve ser >/= 0.",
                            "Cálculo de Distribuição Binomial",
                            JOptionPane.WARNING_MESSAGE);
                    txtNumProvas.requestFocus();
                } catch (probabilidadeInvalidoException excecao) {
                    JOptionPane.showMessageDialog(DialogBinomial.this,
                            "O valor da probabilidade deve pertencer ao intervalo [0;1]!",
                            "Cálculo de Distribuição Binomial",
                            JOptionPane.WARNING_MESSAGE);
                    txtProbabilidade.requestFocus();
                }
            }
        });
        return btn;
    }

    private JButton criarBotaoCancelar() {
        JButton btn = new JButton("Cancelar");
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        return btn;
    }

    private JPanel criarPainelCalculo() {
        JLabel lbl = new JLabel("Valores de X a calcular:", JLabel.RIGHT);
        lbl.setPreferredSize(LABEL_TAMANHO);
        String[] op = new String[]{"≥", "≤", "="};
        jcb = new JComboBox(op);

        final int CAMPO_LARGURA = 5;
        txtValorX = new JTextField(CAMPO_LARGURA);
        txtValorX.requestFocus();

        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl);
        p.add(jcb);
        p.add(txtValorX);

        return p;
    }
}
