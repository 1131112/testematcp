/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import testedistribuicoes.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;

public class DialogMediaPopula extends JDialog {

    private JTextField txtMedia, txtDesvio, txtAmostra;
    private JComboBox jcb;

    private static final int DIALOGO_DESVIO_X = 100, DIALOGO_DESVIO_Y = 100;
    private static final Dimension LABEL_TAMANHO = new JLabel("Intervalo de Confiança(%) =").getPreferredSize();
    
     private static final String IMAGEM_DIST_MEDIA_POP =  "icMedia.PNG";

    public DialogMediaPopula(JFrame pai) {

        super(pai, "Média de Populações", true);

        GridLayout g1 = new GridLayout(5, 1);
        setLayout(g1);

        JPanel p1 = criarPainelMedia();
        JPanel p2 = criarPainelDesvio();
        JPanel p3 = criarPainelAmostra();
        JPanel p4 = criarPainelIC();
        JPanel p5 = criarPainelBotoes();

        add(p1);
        add(p2);
        add(p3);
        add(p4);
        add(p5);

        setLocation(pai.getX() + DIALOGO_DESVIO_X, pai.getY() + DIALOGO_DESVIO_Y);
        pack();
        setResizable(false);
        setVisible(true);
    }

    private JPanel criarPainelMedia() {
        JLabel lbl = new JLabel("E(X)= \u00B5 =", JLabel.RIGHT);
        lbl.setPreferredSize(LABEL_TAMANHO);

        final int CAMPO_LARGURA = 5;
        txtMedia = new JTextField(CAMPO_LARGURA);
        txtMedia.requestFocus();

        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl);
        p.add(txtMedia);
        return p;
    }

    private JPanel criarPainelDesvio() {
        JLabel lbl = new JLabel("Desvio Padrão = \u03C3 =", JLabel.RIGHT);
        lbl.setPreferredSize(LABEL_TAMANHO);
        String[] op = new String[]{">", "≤", "="};

        final int CAMPO_LARGURA = 5;
        txtDesvio = new JTextField(CAMPO_LARGURA);
        txtDesvio.requestFocus();

        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl);
        p.add(txtDesvio);

        return p;
    }

    private JPanel criarPainelBotoes() {
        JButton btnOK = criarBotaoOK();
        getRootPane().setDefaultButton(btnOK);

        JButton btnCancelar = criarBotaoCancelar();

        JPanel p = new JPanel();
        final int MARGEM_SUPERIOR = 0, MARGEM_INFERIOR = 10;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 10;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));
        p.add(btnOK);
        p.add(btnCancelar);

        return p;
    }

    private JButton criarBotaoOK() {
        JButton btn = new JButton("OK");
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    double media = Double.parseDouble(txtMedia.getText());
                    if (media < 0) {
                        throw new poissonMediaInvalidoException();
                    }
                    Double desvio = Double.parseDouble(txtDesvio.getText());
                    int amostra = Integer.parseInt(txtAmostra.getText());
                    if (amostra < 0) {
                        throw new probabilidadeInvalidoException();
                    }
                    IntervalosConfianca ic = new IntervalosConfianca(amostra, media, desvio, jcb.getSelectedIndex());
                    ImageIcon icon = new ImageIcon(IMAGEM_DIST_MEDIA_POP);
                    JOptionPane.showMessageDialog(DialogMediaPopula.this,
                            new JLabel("="+ ic.calcIC(), icon, JLabel.LEFT),
                            "Média de Populações", 
                            JOptionPane.DEFAULT_OPTION);
                    dispose();
                } catch (NumberFormatException excecao) {
                    JOptionPane.showMessageDialog(DialogMediaPopula.this,
                            "Tem que introduzir um valor numérico no numero de provas.",
                            "Média de Populações",
                            JOptionPane.WARNING_MESSAGE);
                    txtMedia.requestFocus();
                } catch (probabilidadeInvalidoException exexcecao) {
                    JOptionPane.showMessageDialog(DialogMediaPopula.this,
                            "O valor da amostra não deve ser negativo!",
                            "Média de Populações",
                            JOptionPane.WARNING_MESSAGE);
                    txtAmostra.requestFocus();
                } catch (poissonMediaInvalidoException ex) {
                    JOptionPane.showMessageDialog(DialogMediaPopula.this,
                            "( E(X)= U+03BC ) ≥ 0 ",
                            "Média de Populações",
                            JOptionPane.WARNING_MESSAGE);
                }
            }
        });
        return btn;
    }

    private JButton criarBotaoCancelar() {
        JButton btn = new JButton("Cancelar");
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        return btn;
    }

    private JPanel criarPainelAmostra() {

        JLabel lbl = new JLabel("Amostra = n =", JLabel.RIGHT);
        lbl.setPreferredSize(LABEL_TAMANHO);

        final int CAMPO_LARGURA = 5;
        txtAmostra = new JTextField(CAMPO_LARGURA);
        txtAmostra.requestFocus();

        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl);
        p.add(txtAmostra);

        return p;

    }

    private JPanel criarPainelIC() {

        JLabel lbl = new JLabel("Intervalo de Confiança(%) =", JLabel.RIGHT);
        lbl.setPreferredSize(LABEL_TAMANHO);
        String[] op = new String[]{"99.73%", "99%", "98%", "96%", "95.45%", "95%", "90%"};
        jcb = new JComboBox(op);

        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl);
        p.add(jcb);

        return p;
    }
}
