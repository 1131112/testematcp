/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import java.awt.Color;
import java.awt.Image;
import java.awt.PopupMenu;
import java.awt.event.*;
import javax.swing.*;

public class Janela extends JFrame {

    private static final int JANELA_POSICAO_X = 200, JANELA_POSICAO_Y = 200;
    private static final int JANELA_LARGURA = 500;
    private static final int JANELA_ALTURA = 500;

    UIManager UI = new UIManager();

    public Janela() {

        super("MATCP - Estatítica");

        setIconImage(getToolkit().createImage("sigmai.png"));
        setSize(JANELA_ALTURA, JANELA_LARGURA);
        JMenuBar menuBar = new JMenuBar();
        setJMenuBar(menuBar);
        menuBar.add(criarMenuDistribuições());
        menuBar.add(criarMenuIntervalosConfianca());
        menuBar.add(criarMenuAjuda());

        add(new JLabel(new ImageIcon("1isep_logo.jpg")));

        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                fechar();
            }
        });

        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);

        UI.put("OptionPane.background", Color.white);
        UI.put("Panel.background", Color.white);

        pack();
        setResizable(false);
        setLocation(JANELA_POSICAO_X, JANELA_POSICAO_Y);
        setVisible(true);

    }

    private JMenu criarMenuDistribuições() {
        JMenu menu = new JMenu("Distribuições");
        menu.setMnemonic(KeyEvent.VK_D);

        menu.add(distrBinomial());
        menu.add(distrPoisson());
        menu.add(distrNormais());
        menu.add(criarItemSair());

        return menu;
    }

    private JMenu criarMenuIntervalosConfianca() {
        JMenu menu = new JMenu("Intervalos de Confiança");
        menu.setMnemonic(KeyEvent.VK_I);

        menu.add(mediasPopolucoes());
        menu.add(difMediasPopulacoes());
        menu.add(proporcoes());
        menu.add(difProporcoes());
        menu.add(criarItemSair());

        return menu;
    }

    private JMenuItem distrBinomial() {
        JMenuItem item = new JMenuItem("Binomial", KeyEvent.VK_B);
        item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_B, InputEvent.CTRL_MASK));
        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                DialogBinomial dialog = new DialogBinomial(Janela.this);
            }
        });

        return item;
    }

    private JMenuItem distrPoisson() {
        JMenuItem item = new JMenuItem("Poisson", KeyEvent.VK_P);
        item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_P, InputEvent.CTRL_MASK));
        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                DialogPoisson dialog = new DialogPoisson(Janela.this);
            }
        });

        return item;
    }

    private JMenuItem mediasPopolucoes() {
        JMenuItem item = new JMenuItem("Média de Populações", KeyEvent.VK_1);
        item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_1, InputEvent.CTRL_MASK));
        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                DialogMediaPopula dialog = new DialogMediaPopula(Janela.this);
            }
        });

        return item;
    }

    private JMenuItem difMediasPopulacoes() {
        JMenuItem item = new JMenuItem("Diferença de Média de Populações", KeyEvent.VK_2);
        item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_2, InputEvent.CTRL_MASK));
        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                DialogDifMediaPopula dialog = new DialogDifMediaPopula(Janela.this);
            }
        });

        return item;
    }

    private JMenuItem proporcoes() {
        JMenuItem item = new JMenuItem("Proporções", KeyEvent.VK_3);
        item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_3, InputEvent.CTRL_MASK));
        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                DialogMediaPropor dialog = new DialogMediaPropor(Janela.this);
            }
        });

        return item;
    }

    private JMenuItem difProporcoes() {
        JMenuItem item = new JMenuItem("Diferença de Proporções", KeyEvent.VK_4);
        item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_4, InputEvent.CTRL_MASK));
        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                DialogDifMediaPropor dialog = new DialogDifMediaPropor(Janela.this);
            }
        });

        return item;
    }

    private JMenuItem criarItemSair() {
        JMenuItem item = new JMenuItem("Sair", KeyEvent.VK_S);
        item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.ALT_MASK));
        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fechar();
            }
        });
        return item;
    }

    private JMenu criarMenuAjuda() {
        JMenu menu = new JMenu("Ajuda");
        menu.setMnemonic(KeyEvent.VK_A);

        menu.add(criarItemAcerca());

        return menu;
    }

    private JMenuItem criarItemAcerca() {
        JMenuItem item = new JMenuItem("Acerca", KeyEvent.VK_C);

        item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_4, InputEvent.CTRL_MASK));
        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                DialogAcerca dialog = new DialogAcerca(Janela.this);
            }
        });

        return item;
    }

    private void fechar() {
        String[] opSimNao = {"Sim", "Não"};
        int resposta = JOptionPane.showOptionDialog(this,
                "Deseja fechar a aplicação?",
                "MATCP - Estatística",
                0,
                JOptionPane.QUESTION_MESSAGE,
                null,
                opSimNao,
                opSimNao[1]);

        final int SIM = 0;
        if (resposta == SIM) {
            dispose();
        }
    }

    private JMenuItem distrNormais() {
        JMenuItem item = new JMenuItem("Normal", KeyEvent.VK_N);
        item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, InputEvent.CTRL_MASK));
        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                DialogNormais dialog = new DialogNormais(Janela.this);
            }
        });

        return item;
    }

}
