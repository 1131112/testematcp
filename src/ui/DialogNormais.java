/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import java.awt.*;
import java.awt.event.*;
import java.text.DecimalFormat;
import javax.swing.*;
import javax.swing.border.*;
import testedistribuicoes.*;

public class DialogNormais extends JDialog {

    private JTextField txtMedia, txtVariancia, txtParametro;
    private JComboBox jcb;

    private static final int DIALOGO_DESVIO_X = 100, DIALOGO_DESVIO_Y = 100;
    private static final Dimension LABEL_TAMANHO = new JLabel("Valores de X a calcular:").getPreferredSize();

    private static final String IMAGEM_DIST_NORMAL = "NormalReduzida.PNG";

    DecimalFormat df = new DecimalFormat("#.####");

    public DialogNormais(Janela pai) {

        super(pai, "Distribuição Normal", true);

        GridLayout g1 = new GridLayout(4, 1);
        setLayout(g1);

        JPanel p1 = criarPainelMedia();
        JPanel p2 = criarPainelVariancia();
        JPanel p3 = criarPainelParametro();
        JPanel p4 = criarPainelBotoes();

        add(p1);
        add(p2);
        add(p3);
        add(p4);

        setLocation(pai.getX() + DIALOGO_DESVIO_X, pai.getY() + DIALOGO_DESVIO_Y);
        pack();
        setResizable(false);
        setVisible(true);
    }

    private JPanel criarPainelMedia() {
        JLabel lbl = new JLabel("E(X)= \u00B5 =", JLabel.RIGHT);
        lbl.setPreferredSize(LABEL_TAMANHO);

        final int CAMPO_LARGURA = 5;
        txtMedia = new JTextField(CAMPO_LARGURA);
        txtMedia.requestFocus();

        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl);
        p.add(txtMedia);
        return p;
    }

    private JPanel criarPainelVariancia() {
        JLabel lbl = new JLabel("V(X)= \u03C3\u00B2 =", JLabel.RIGHT);
        lbl.setPreferredSize(LABEL_TAMANHO);

        final int CAMPO_LARGURA = 5;
        txtVariancia = new JTextField(CAMPO_LARGURA);
        txtVariancia.requestFocus();

        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl);
        p.add(txtVariancia);
        return p;
    }

    private JPanel criarPainelParametro() {
        JLabel lbl = new JLabel("Valores de X a calcular:", JLabel.RIGHT);
        lbl.setPreferredSize(LABEL_TAMANHO);
        String[] op = new String[]{">", "≤"};
        jcb = new JComboBox(op);

        final int CAMPO_LARGURA = 5;
        txtParametro = new JTextField(CAMPO_LARGURA);
        txtParametro.requestFocus();

        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl);
        p.add(jcb);
        p.add(txtParametro);

        return p;
    }

    private JPanel criarPainelBotoes() {
        JButton btnOK = criarBotaoOK();
        getRootPane().setDefaultButton(btnOK);

        JButton btnCancelar = criarBotaoCancelar();

        JPanel p = new JPanel();
        final int MARGEM_SUPERIOR = 0, MARGEM_INFERIOR = 10;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 10;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));
        p.add(btnOK);
        p.add(btnCancelar);

        return p;
    }

    private JButton criarBotaoOK() {
        JButton btn = new JButton("OK");
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    double media = Double.parseDouble(txtMedia.getText());
                    double parametro = Double.parseDouble(txtParametro.getText());
                    double variancia = Double.parseDouble(txtVariancia.getText());
                    Normal n = new Normal(media, variancia, parametro);

                    ImageIcon icon = new ImageIcon(IMAGEM_DIST_NORMAL);
                    if (jcb.getSelectedIndex() == 0) {
                       
                        if (n.calcReduzida() < 0) {
                            Double result = 1 - (1 - n.calcDistribuicao());

                            JOptionPane.showMessageDialog(DialogNormais.this,
                                    new JLabel("=" + df.format(result), icon, JLabel.LEFT),
                                    "Distribuição Normal Reduzida",
                                    JOptionPane.DEFAULT_OPTION);
                        } else {
                            Double result = 1 - n.calcDistribuicao();

                            JOptionPane.showMessageDialog(DialogNormais.this,
                                    new JLabel("=" + df.format(result), icon, JLabel.LEFT),
                                    "Distribuição Normal Reduzida",
                                    JOptionPane.DEFAULT_OPTION);
                        }
                    } else {

                        if (n.calcReduzida() < 0) {
                            Double result = 1 - n.calcDistribuicao();

                            JOptionPane.showMessageDialog(DialogNormais.this,
                                    new JLabel("=" + df.format(result), icon, JLabel.LEFT),
                                    "Distribuição Normal Reduzida",
                                    JOptionPane.DEFAULT_OPTION);
                        } else {
                            Double result = n.calcDistribuicao();

                            JOptionPane.showMessageDialog(DialogNormais.this,
                                    new JLabel("=" + df.format(result), icon, JLabel.LEFT),
                                    "Distribuição Normal Reduzida",
                                    JOptionPane.DEFAULT_OPTION);
                        }

                    }

                    dispose();
                } catch (NumberFormatException excecao) {
                    JOptionPane.showMessageDialog(DialogNormais.this,
                            "Tem que introduzir um valor numérico.",
                            "Distribuição Normal Reduzida",
                            JOptionPane.WARNING_MESSAGE);
                    txtVariancia.requestFocus();

                }
            }
        });
        return btn;
    }

    private JButton criarBotaoCancelar() {
        JButton btn = new JButton("Cancelar");
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        return btn;
    }

}
