package testedistribuicoes;

public class Poisson {

    private double p;
    private int x;

    public Poisson(double p, int x) {
        this.p = p;
        this.x = x;
    }

    public double getP() {
        return p;
    }

    public void setP(double p) {
        this.p = p;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    //((e^-L) * L^x)/x!
    public double calcProbabilidade() {
        return (Math.pow(Math.E, -p) * Math.pow(p, x)) / Fatorial.calc(x);
    }

    public double calcDistribuicao() {
        double soma = 0;
        for (int i = 0; i < getX() + 1; i++) {
            soma = soma + ((Math.pow(Math.E, -p) * Math.pow(p, i)) / Fatorial.calc(i));
        }
        return soma;
    }

    public double calcDistSomada() {
        double soma = 0;
        for (int i = getX(); i < 21; i++) {
            soma = soma + ((Math.pow(Math.E, -p) * Math.pow(p, i)) / Fatorial.calc(i));
        }
        return soma;
    }
}
