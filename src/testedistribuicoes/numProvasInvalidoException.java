package testedistribuicoes;

public class numProvasInvalidoException extends Exception {

    public numProvasInvalidoException() {

        super("Numero de Provas Inválido!!!");
    }

    public numProvasInvalidoException(String mensagem) {
        super(mensagem);
    }
}
