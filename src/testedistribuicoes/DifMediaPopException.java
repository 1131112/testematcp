package testedistribuicoes;

public class DifMediaPopException extends Exception {

    public DifMediaPopException() {
        super("( E(X)= \u00B5 ) ≥ 0");
    }

    public DifMediaPopException(String mensagem) {
        super(mensagem);
    }

}
