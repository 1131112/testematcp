package testedistribuicoes;

import javax.swing.JLabel;

public class probabilidadeInvalidoException extends Exception {

    public probabilidadeInvalidoException() {
        super("Probabilidade deve pertencer ao intervalo [0;1]");
    }
    
    public probabilidadeInvalidoException(String mensagem){
        super(mensagem);
    }

    
    
    
    
}
