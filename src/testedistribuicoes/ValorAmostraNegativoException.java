/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package testedistribuicoes;

/**
 *
 * @author Pedro
 */
public class ValorAmostraNegativoException extends Exception {

    /**
     * Creates a new instance of <code>ValorAmostraNegativoException</code>
     * without detail message.
     */
    public ValorAmostraNegativoException() {
         super("A amostra deve ser ≥ 0");
    }
    
    public ValorAmostraNegativoException(String mensagem){
        super(mensagem);
    }

}
