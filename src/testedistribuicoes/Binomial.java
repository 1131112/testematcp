package testedistribuicoes;

public class Binomial {

    /**
     * Probabilidade de sucesso
     */
    private double p;
    /**
     * nº de casos de sucesso
     */
    private int n;

    private int x;

    public Binomial(double p, int n, int x) {
        this.p = p;
        this.n = n;
        this.x = x;
    }

    public double getP() {
        return p;
    }

    public void setP(double p) {
        this.p = p;
    }

    public int getN() {
        return n;
    }

    public void setN(int n) {
        this.n = n;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    //nCx - combinações n, x a x
    private double combinacao(int n, int x) {

        return Fatorial.calc(n) / (Fatorial.calc(x) * Fatorial.calc(n - x));
    }

// nCx * p^x * (1-p)^n-x;

    public double calcProbabilidade() {
        return combinacao(n, x) * Math.pow(getP(), getX()) * Math.pow(1 - getP(), getN() - getX());
    }

    public double calcDistribuicao() {
        double soma = 0;
        for (int i = 0; i < getX() + 1; i++) {
            soma = soma + combinacao(getN(), i) * Math.pow(getP(), i) * Math.pow(1 - getP(), getN() - i);
        }
        return soma;
    }

    public double calcDistSomada() {
        double soma = 0;
        for (int i = getX(); i < getN() + 1; i++) {
            soma = soma + combinacao(getN(), i) * Math.pow(getP(), i) * Math.pow(1 - getP(), getN() - i);
        }
        return soma;
    }
}
