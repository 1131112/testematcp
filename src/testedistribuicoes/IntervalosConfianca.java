package testedistribuicoes;
//para intervalos de Confiança vamos precisar das seguintes variaveis:
// n - numero da amostra

import java.text.DecimalFormat;

// x - média de alturas
// d - desvio padrao
// ic - % dos intervalos d confiança
public class IntervalosConfianca {

    private int n;
    private double x;
    private double d;
    private int z;

    private static final double[] COEFICIENTE_ZC
            = {3, 2.58, 2.33, 2.05, 2, 1.96, 1.645};

    public IntervalosConfianca(int n, double x, double d, int z) {
        this.n = n;
        this.x = x;
        this.d = d;
        this.z = z;
    }

    public int getN() {
        return n;
    }

    public void setN(int n) {
        this.n = n;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getD() {
        return d;
    }

    public void setD(double d) {
        this.d = d;
    }

    public int getZ() {
        return z;
    }

    public void setZ(int z) {
        this.z = z;
    }

    private double calcZc() {

        return COEFICIENTE_ZC[getZ()];

    }

    public String calcIC() {

        DecimalFormat f = new DecimalFormat("0.00");

        double min = getX() - calcZc() * getD() / Math.sqrt(getN());
        double max = getX() + calcZc() * getD() / Math.sqrt(getN());

        return ("[" + f.format(min) + ";" + f.format(max) + "]");
    }

}
