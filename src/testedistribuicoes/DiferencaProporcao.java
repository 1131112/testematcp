package testedistribuicoes;

import java.text.DecimalFormat;

public class DiferencaProporcao {

    private static final double[] COEFICIENTE_ZC
            = {3, 2.58, 2.33, 2.05, 2, 1.96, 1.645};

    public DiferencaProporcao() {
    }

    private double calcZc(Proporcao p) {
        return COEFICIENTE_ZC[p.getZ()];
    }

    public String calcDiferencaProporcao(Proporcao p1, Proporcao p2) {

        DecimalFormat f = new DecimalFormat("0.00");

        double min = (p1.calcP() - p2.calcP()) - calcZc(p1) * Math.sqrt((p1.calcP() * p1.calcQ() / p1.getN()) + (p2.calcP() * p2.calcQ() / p2.getN()));
        double max = (p1.calcP() - p2.calcP()) + calcZc(p1) * Math.sqrt((p1.calcP() * p1.calcQ() / p1.getN()) + (p2.calcP() * p2.calcQ() / p2.getN()));

        return "[" + f.format(min) + ";" + f.format(max) + "]";
    }

}
