package testedistribuicoes;

public class GrauConfianca {
    private static final double[] GRAU_CONFIANCA
            = {99.73, 99,  98, 96, 95.45, 95, 90};
    
    private static final double[] COEFICIENTE_ZC
            = {3, 2.58, 2.33, 2.05, 2, 1.96, 1.645};

    public static double[] getGRAU_CONFIANCA() {
        return GRAU_CONFIANCA;
    }

    public static double[] getCOEFICIENTE_ZC() {
        return COEFICIENTE_ZC;
    }    
    
}
