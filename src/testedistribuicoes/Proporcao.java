package testedistribuicoes;

import java.text.DecimalFormat;

public class Proporcao {

    private double n;
    private double f;
    private int z;
    private static final double[] COEFICIENTE_ZC
            = {3, 2.58, 2.33, 2.05, 2, 1.96, 1.645};

    public Proporcao(int n, int f, int z) {
        this.n = n;
        this.f = f;
        this.z = z;
    }

    public double getN() {
        return n;
    }

    public void setN(int n) {
        this.n = n;
    }

    public double getF() {
        return f;
    }

    public void setF(int f) {
        this.f = f;
    }

    public int getZ() {
        return z;
    }

    public void setZ(int z) {
        this.z = z;
    }

    public double calcP() {
        double p = (getF() / getN());
        return p;
    }

    public double calcQ() {
        double q = 1 - calcP();
        return q;
    }

    private double calcZc() {
        
        return COEFICIENTE_ZC[getZ()];

    }

    public String calcIC() {

        DecimalFormat f = new DecimalFormat("0.00");

        double min = calcP() - calcZc() * Math.sqrt(calcP() * calcQ() / getN());
        double max = calcP() + calcZc() * Math.sqrt(calcP() * calcQ() / getN());

        return "[" + f.format(min) + "; " + f.format(max) + "]";
    }

}
