package testedistribuicoes;

import java.text.DecimalFormat;

public class DiferencaIC {
    private static final double[] COEFICIENTE_ZC
            = {3, 2.58, 2.33, 2.05, 2, 1.96, 1.645};
    public DiferencaIC() {
    }

    private double calcZc(IntervalosConfianca ic1) {
        return COEFICIENTE_ZC[ic1.getZ()];
    }

    public String calcDiferenca(IntervalosConfianca ic1, IntervalosConfianca ic2) {

        DecimalFormat f = new DecimalFormat("0.00");

        double min = (ic1.getX() - ic2.getX()) - calcZc(ic1) * Math.sqrt((Math.pow(ic1.getD(), 2) / ic1.getN()) + (Math.pow(ic2.getD(), 2) / ic2.getN()));
        double max = (ic1.getX() - ic2.getX()) + calcZc(ic1) * Math.sqrt((Math.pow(ic1.getD(), 2) / ic1.getN()) + (Math.pow(ic2.getD(), 2) / ic2.getN()));

        return "[" + f.format(min) + "; " + f.format(max) + "]";
    }

}
