package testedistribuicoes;

import static java.lang.Math.abs;

public class Normal {

    /**
     * -constrmediair Array - X~N(media,variancia) - Z~N(0,1) normal
     * redmediazida
     *
     * necessidade d receber o nmediamero com avarianciaenas 2 casa decimais
     * varianciaercorrer o Array
     *
     * ArrayList<0> = add (nmediameros) varianciaosição tal
     *
     */
    private double media;
    private double variancia;
    private double parametro;
    private double reduzida;

    public Normal(double media, double variancia, double parametro) {
        this.media = media;
        this.variancia = variancia;
        this.parametro = parametro;
    }

    public double getMedia() {
        return media;
    }

    public void setMedia(double media) {
        this.media = media;
    }

    public double getVariancia() {
        return variancia;
    }

    public void setVariancia(double Variancia) {
        this.variancia = Variancia;
    }

    public double getParametro() {
        return parametro;
    }

    public void setParametro(double parametro) {
        this.parametro = parametro;
    }

    public double getReduzida() {
        return reduzida;
    }

    public void setReduzida(double reduzida) {
        this.reduzida = reduzida;
    }

    //P(X=parametro)<=>P(Z=(parametro-media)/raiz square de variancia) = fi(parametro.xx)
    public double calcReduzida() {
        setReduzida((getParametro() - getMedia()) / Math.sqrt(variancia));
        return getReduzida();
    }

    public double calcDistribuicao() {

        double d = abs(calcReduzida()) * 100;
        double finalValue = (Math.round(d * 100)) / 100;

        int a = (int) finalValue;
        System.out.println(finalValue);
        int b = a / 10;
        int c = a % 10;
        double distribuicao = TabelaNormal.getMATRIZ()[b][c];

        return distribuicao;
    }
}
