package testedistribuicoes;

public class Fatorial {
    
        public static long calc(int n) {
        long total = 1;

        for (int i = 1; i <= n; i++) {
            total = total * i;
        }
        return total;
    }
}
