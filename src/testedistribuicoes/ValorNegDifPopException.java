/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package testedistribuicoes;

/**
 *
 * @author Pedro
 */
public class ValorNegDifPopException extends Exception {
    public ValorNegDifPopException() {
        super("( E(X)= \u00B5 ) ≥ 0");
    }

    public ValorNegDifPopException(String mensagem) {
        super(mensagem);
    }
    
}
