/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package testedistribuicoes;

/**
 *
 * @author Pedro
 */
public class NumFavoravelInvalidoException extends Exception {

    /**
     * Creates a new instance of <code>NumFavoravelInvalidoException</code>
     * without detail message.
     */
    public NumFavoravelInvalidoException() {
        super("Nº de casos favoráveis deve ser ≥ 0");
    }

    public NumFavoravelInvalidoException(String mensagem) {
        super(mensagem);
    }
}
